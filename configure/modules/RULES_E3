# -*- mode: Makefile;-*-
.NOTPARALLEL:

.DEFAULT_GOAL := help

.PHONY: help default install uninstall build rebuild clean conf all prebuild

default: help

# # help is defined in 
# # https://gist.github.com/rcmachado/af3db315e31383502660
help:
	$(info --------------------------------------- ) 
	$(info Available targets)
	$(info --------------------------------------- )
	$(QUIET) awk '/^[a-zA-Z\-\_0-9]+:/ {            \
	  nb = sub( /^## /, "", helpMsg );              \
	  if(nb == 0) {                                 \
	    helpMsg = $$0;                              \
	    nb = sub( /^[^:]*:.* ## /, "", helpMsg );   \
	  }                                             \
	  if (nb)                                       \
	    print  $$1 "\t" helpMsg;                    \
	}                                               \
	{ helpMsg = $$0 }'                              \
	$(MAKEFILE_LIST) | column -ts: 




## Install current module to $(EPICS_BASE)/require/$(E3_REQUIRE_VERSION)/siteMods
install: install_module install_links vlibs

install_module: build db
	$(QUIET) $(E3_MODULE_MAKE_CMDS) install

## Uninstall the current module
uninstall: conf
	$(QUIET) $(E3_MODULE_MAKE_CMDS) uninstall

## Build current module
build: conf checkout prebuild
	$(QUIET) $(E3_MODULE_MAKE_CMDS) build

## Run module-specific commands before building
prebuild: conf
	$(QUIET) $(E3_MODULE_MAKE_CMDS) prebuild

## Displays information about the build process
debug: conf
	$(QUIET) $(E3_MODULE_MAKE_CMDS) debug

## Clean, build, and install the current module
rebuild: clean build install 

## Deletes temporary build files
clean: conf
	$(QUIET) $(E3_MODULE_MAKE_CMDS) clean

## Initializes, patches, builds, and installs
all: init patch rebuild

# Copy $(E3_MODULE_MAKEFILE) into $(E3_MODULE_SRC_PATH)
conf: module_name_check
	$(QUIET) install -p -m 644 $(TOP)/$(E3_MODULE_MAKEFILE)  $(E3_MODULE_SRC_PATH)/

# We should check that the module name satisfies a few conditions before we go on.
module_name_check:
ifneq ($(shell echo $(E3_MODULE_NAME) | grep -q '^[a-z_][a-z0-9_]\+$$' ; echo $$?),0)
	$(error E3_MODULE_NAME '$(E3_MODULE_NAME)' is not valid. It should consist only of lowercase letters, numbers, and underscores.)
endif

.PHONY: init git-submodule-sync $(E3_MODULE_SRC_PATH)  checkout

ifeq (,$(strip $(EPICS_MODULE_TAG)))
E3_LOCAL_SOURCE:=1
endif
ifneq (,$(findstring -loc,$(E3_MODULE_SRC_PATH)))
$(warning DEPRECATED: Local source mode "-loc" is being deprecated. Please comment out the EPICS_MODULE_TAG to use local source mode.)
E3_LOCAL_SOURCE:=1
endif

ifeq ($(E3_LOCAL_SOURCE),1)
init: 
	$(QUIET) echo ">> You are in the local source mode."
	$(QUIET) echo ">> Nothing happens."

checkout:

else
## Syncs and checks out the tag $(EPICS_MODULE_TAG) for the submodule 
init: git-submodule-sync $(E3_MODULE_SRC_PATH)  checkout


git-submodule-sync:
	$(QUIET) git submodule sync


$(E3_MODULE_SRC_PATH): 
	$(QUIET) $(git_update)


checkout: 
	cd $(E3_MODULE_SRC_PATH) && git checkout $(EPICS_MODULE_TAG)


endif

